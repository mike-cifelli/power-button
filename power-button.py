from gpiozero import Button
from signal import pause
from subprocess import check_call

BUTTON_GPIO_PIN = 3
HOLD_TIME_IN_SECONDS = 2


def poweroff():
    check_call(['sudo', 'poweroff'])


button = Button(BUTTON_GPIO_PIN, hold_time=HOLD_TIME_IN_SECONDS)
button.when_held = poweroff

pause()
