# power-button

A Raspberry Pi project to allow a button to turn off (and potentially turn on) the system.

## Dependencies
```
sudo apt install python3-gpiozero
```

## Installation
```
sudo ./install
```
